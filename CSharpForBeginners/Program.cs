﻿using System;

namespace CSharpForBeginners
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine($"Bishop={ChessMasterBishop(1, 1, 6, 8)}");
        }

        /*
        * на шахматній дошці у клітинці з координатами (a,b) знаходиться слон(офіцер),
        * створіть функцію що буде повертати істину
        якщо клітина (c,d) знаходиться під боєм
         */
        public static bool ChessMasterBishop(byte a, byte b, byte c, byte d)
        {
           // return false;
            throw new NotImplementedException();
        }

        /*
         на шахматній дошці у клітинці з координатами (a,b) знаходиться тура(ладья),
         створіть функцію що буде повертати істину
         якщо клітина (c,d) знаходиться під боєм
        */
        public static bool ChessMasterCastle(byte a, byte b, byte c, byte d)
        {
            throw new NotImplementedException();
        }

        /*Необхідно обчислити скільки разів у числі number зустрічається цифра digit*/
        public static int CountDigitOccuresInNumber(int number, int digit)
        {
            throw new NotImplementedException();
        }

        /*
           Дано число n необхідно сформувати зубчастий масив наступним чином
           1
           1  2
           1  2  3
           1  2  3  4
           .............
           1  2  3  4 ..... n
        */

        public static int[][] JaggedArray(int n)
        {
            throw new NotImplementedException();
        }

        /*
         * Дан масив  inputArray.
         * За допомогою класу Array відсортуйте його в порядку зростання його елементів
         */
        public static int[] SortArray(int[] inputArray)
        {
            throw new NotImplementedException();
        }
    }
}
